﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LexicalAnalizerModels.BusinessLogic
{
	public class Lexem
	{
		public LexemType Type { get; set; }
		public int Index { get; set; }
		public override string ToString()
		{
			return new StringBuilder().Append("(")
									  .Append((int)Type)
									  .Append(", ")
									  .Append(Index)
									  .Append(")")
									  .ToString();
		}
	}
}
