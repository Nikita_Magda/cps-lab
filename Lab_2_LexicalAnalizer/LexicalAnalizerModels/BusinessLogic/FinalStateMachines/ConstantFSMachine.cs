﻿using System.Collections.Generic;
using System.Linq;

namespace LexicalAnalizerModels.BusinessLogic.FinalStateMachines
{
	public class ConstantFSMachine : FinalStateMachine
	{
		//0  -> 1 [0], 2 [1-9]
		//1f -> 3 [.], 2 [0-9]
		//2f -> 2 [0-9], 3 [.]
		//3  -> 4 [0-9]
		//4f -> 4 [0-9]

		public ConstantFSMachine(IEnumerable<char> numberSymbols)
		{
			AnalizeLexemType = LexemType.Constant;
			StateTransfersTable = new Dictionary<int, List<State>>
			{
				{
					0,
					new List<State>
					{
						new State(1, true, new char[] {'0'}),
						new State(2, true, numberSymbols.Skip(1))
					}
				},
				{
					1,
					new List<State>
					{
						new State(3, true, new char[] {'.'}),
						new State(2, true, numberSymbols)
					}
				},
				{
					2,
					new List<State>
					{
						new State(2, true, numberSymbols),
						new State(3, false, new char[] {'.'}),
					}	
				},
				{
					3,
					new List<State>
					{
						new State(4, true, numberSymbols)
					}
				},
				{
					4,
					new List<State>
					{
						new State(4, true, numberSymbols)
					}
				}
			};
		}
	}
}
