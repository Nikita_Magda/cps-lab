﻿using System.Collections.Generic;

namespace LexicalAnalizerModels.BusinessLogic.FinalStateMachines
{
	public class OperationFSMachine : FinalStateMachine
	{
		//0  -> 1 [d], 2 [w], 6 [+|-|*|/|<|>|=|,]
		//1	 -> 6 [o]
		//2  -> 3 [h]
		//3  -> 4 [i]
		//4  -> 5 [l]
		//5  -> 6 [e]
		//6f 
		public OperationFSMachine(IEnumerable<char> operationSymbols)
		{
			AnalizeLexemType = LexemType.Operation;
			StateTransfersTable = new Dictionary<int, List<State>>
			{
				{
					0,
					new List<State>
					{
						new State(1, false, new char[] {'d'}),
						new State(2, false, new char[] {'w'}),
						new State(6, true, operationSymbols)
					}
				},
				{
					1,
					new List<State>
					{
						new State(6, true, new char[] {'o'})
					}
				},
				{
					2,
					new List<State>
					{
						new State(3, false, new char[] {'h'})
					}
				},
				{
					3,
					new List<State>
					{
						new State(4, false, new char[] {'i'})
					}
				},
				{
					4,
					new List<State>
					{
						new State(5, false, new char[] {'l'})
					}
				},
				{
					5,
					new List<State>
					{
						new State(6, true, new char[] {'e'})
					}
				},
				{
					6,
					new List<State>()
				}
			};
		}
	}
}
