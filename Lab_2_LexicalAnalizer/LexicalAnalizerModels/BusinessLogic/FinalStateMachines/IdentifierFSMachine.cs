﻿using System.Collections.Generic;
using System.Linq;

namespace LexicalAnalizerModels.BusinessLogic.FinalStateMachines
{
	public class IdentifierFSMachine : FinalStateMachine
	{
		//0  -> 1 [a-zA-Z]
		//1f -> 1 [a-zA-Z0-9]

		public IdentifierFSMachine(IEnumerable<char> alphabetSymbols, IEnumerable<char> numberSymbols)
		{
			AnalizeLexemType = LexemType.Identifier;
			StateTransfersTable = new Dictionary<int, List<State>>
			{
				{
					0,
					new List<State>
					{
						new State(1, true, alphabetSymbols)
					}
				},
				{
					1,
					new List<State>
					{
						new State(1, true, alphabetSymbols.Concat(numberSymbols))
					}
				}
			};
		}
	}
}
