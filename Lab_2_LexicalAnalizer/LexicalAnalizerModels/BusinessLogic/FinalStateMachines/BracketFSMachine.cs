﻿using System.Collections.Generic;


namespace LexicalAnalizerModels.BusinessLogic.FinalStateMachines
{
	public class BracketFSMachine : FinalStateMachine
	{
		//0  -> 1f [(|)|[|]]
		public BracketFSMachine()
		{
			AnalizeLexemType = LexemType.Bracket;
			StateTransfersTable = new Dictionary<int, List<State>>
			{
				{
					0,
					new List<State>
					{
						new State(1, true, new char[] {'('}),
						new State(1, true, new char[] {')'}),
						new State(1, true, new char[] {'['}),
						new State(1, true, new char[] {']'})
					}
				},
				{
					1,
					new List<State>()
				}
			};
		}
	}
}
