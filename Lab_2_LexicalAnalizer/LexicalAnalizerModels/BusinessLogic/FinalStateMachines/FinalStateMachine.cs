﻿using System.Collections.Generic;

namespace LexicalAnalizerModels.BusinessLogic.FinalStateMachines
{
	public abstract class FinalStateMachine
	{
		public LexemType AnalizeLexemType { get; set; }

		public Dictionary<int, List<State>> StateTransfersTable { get; set; }

		public virtual int GetLexemLength(string input, int idx)
		{
			if(input[idx] == '[')
			{
				int i = 0;
			}
			int nextState = 0;
			int currentState = 0;
			int finalLength = 0;
			int startIdx = idx;
			while (idx < input.Length && nextState < StateTransfersTable[currentState].Count)
			{
				State temp = StateTransfersTable[currentState][nextState];
				if (temp.TransferSymbols.Contains(input[idx]))
				{
					currentState = temp.Number;
					nextState = 0;
					idx++;
					if (temp.Final)
					{
						finalLength = idx - startIdx;
					}
					
				}
				else nextState++;
			}
			return finalLength;
		}
	}
}
