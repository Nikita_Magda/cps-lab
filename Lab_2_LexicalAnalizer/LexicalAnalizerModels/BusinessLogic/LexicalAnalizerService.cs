﻿using System;
using System.Collections.Generic;
using LexicalAnalizerModels.BusinessLogic.FinalStateMachines;
using System.Text;

namespace LexicalAnalizerModels.BusinessLogic
{
	public class LexicalAnalizerService
	{
		public static  Dictionary<LexemType, List<string>> LexemTables { get; set; }

		public static List<FinalStateMachine> FinalStateMachines { get; set; }

		public static List<char> AlphabetTermSymbols = new List<char> {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
																	   'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',};
		public static List<char> NumberTermSymbols = new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

		public static List<char> OperationTermSymbols = new List<char> { '+', '-', '*', '/', '>', '<', '=', ',', '#'};

		public LexicalAnalizerService()
		{
			FinalStateMachines = new List<FinalStateMachine>
			{
				new OperationFSMachine(OperationTermSymbols),
				new IdentifierFSMachine(AlphabetTermSymbols, NumberTermSymbols),
				new ConstantFSMachine(NumberTermSymbols),
				new BracketFSMachine()
			};
			LexemTables = new Dictionary<LexemType, List<string>>
			{
				{ LexemType.Operation, new List<string>() },
				{ LexemType.Identifier, new List<string>() },
				{ LexemType.Constant, new List<string>() },
				{ LexemType.Bracket, new List<string>() }
			};
		}

		public List<Lexem> Analize(string input)
		{
			List<Lexem> res = new List<Lexem>();
			for (int i = 0; i < input.Length; i++)
			{
				if (input[i] != ' ')
				{
					Lexem lexem = new Lexem();
					int lexemLength = 0;
					foreach(var machine in FinalStateMachines)
					{
						int temp = machine.GetLexemLength(input, i);
						if(temp > lexemLength)
						{
							lexemLength = temp;
							lexem.Type = machine.AnalizeLexemType;
						}
					}
					if(lexemLength == 0)
					{
						throw new ArgumentException("Wrong format. The input string contains ivalid symbols.");
					}
					string lexemValue = input.Substring(i, lexemLength);
					int existIndex = LexemTables[lexem.Type].IndexOf(lexemValue);
					if(existIndex < 0)
					{
						lexem.Index = LexemTables[lexem.Type].Count;
						LexemTables[lexem.Type].Add(lexemValue);
					}
					else
					{
						lexem.Index = existIndex;
					}
					i += lexemLength - 1;
					res.Add(lexem);
				}
			}
			return res;
		}

		public string PrintTables()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var table in LexemTables)
			{
				sb.Append((int)table.Key)
				  .Append(" ")
				  .Append(table.Key.ToString())
				  .Append(": \r\n");
				for(int i = 0; i < table.Value.Count; i++)
				{
					sb.Append("\t")
					  .Append(i)
					  .Append(") ")
					  .Append(table.Value[i])
					  .Append("\r\n");
				}
			}
			sb.Append("\r\n");
			return sb.ToString();
		}

		public string PrintOutputLexems(IEnumerable<Lexem> lexems)
		{
			StringBuilder sb = new StringBuilder();
			foreach(Lexem lexem in lexems)
			{
				sb.Append(lexem.ToString());
			}
			return sb.ToString();
		}

	}
}
