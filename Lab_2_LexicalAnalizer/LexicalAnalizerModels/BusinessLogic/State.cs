﻿using System.Collections.Generic;

namespace LexicalAnalizerModels.BusinessLogic
{
	public class State
	{
		public int Number;
		public bool Final;
		//symbols to come to this state from another particular state
		public List<char> TransferSymbols;

		public State(int num, bool isFinal, IEnumerable<char> symbols)
		{
			Number = num;
			Final = isFinal;
			TransferSymbols = new List<char>(symbols);
		}
	}
}
