﻿namespace LexicalAnalizerModels.BusinessLogic
{
	public enum LexemType
	{
		Operation = 1,
		Identifier,
		Constant,		
		Bracket
	}
}
