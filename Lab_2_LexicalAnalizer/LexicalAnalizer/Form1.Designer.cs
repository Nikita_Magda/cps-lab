﻿namespace LexicalAnalizer
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBoxInput = new System.Windows.Forms.TextBox();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonParse = new System.Windows.Forms.Button();
			this.textBoxOutput = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxTable = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxSyntaxicalAnalizerOutput = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonSyntaxicAnalize = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBoxInput
			// 
			this.textBoxInput.Location = new System.Drawing.Point(12, 12);
			this.textBoxInput.Multiline = true;
			this.textBoxInput.Name = "textBoxInput";
			this.textBoxInput.Size = new System.Drawing.Size(362, 81);
			this.textBoxInput.TabIndex = 0;
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(12, 99);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(179, 23);
			this.buttonClear.TabIndex = 1;
			this.buttonClear.Text = "Clear";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// buttonParse
			// 
			this.buttonParse.Location = new System.Drawing.Point(197, 99);
			this.buttonParse.Name = "buttonParse";
			this.buttonParse.Size = new System.Drawing.Size(179, 23);
			this.buttonParse.TabIndex = 2;
			this.buttonParse.Text = "Parse";
			this.buttonParse.UseVisualStyleBackColor = true;
			this.buttonParse.Click += new System.EventHandler(this.buttonParse_Click);
			// 
			// textBoxOutput
			// 
			this.textBoxOutput.Location = new System.Drawing.Point(12, 154);
			this.textBoxOutput.Multiline = true;
			this.textBoxOutput.Name = "textBoxOutput";
			this.textBoxOutput.ReadOnly = true;
			this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
			this.textBoxOutput.Size = new System.Drawing.Size(179, 192);
			this.textBoxOutput.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 138);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(42, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Output:";
			// 
			// textBoxTable
			// 
			this.textBoxTable.Location = new System.Drawing.Point(198, 154);
			this.textBoxTable.Multiline = true;
			this.textBoxTable.Name = "textBoxTable";
			this.textBoxTable.ReadOnly = true;
			this.textBoxTable.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxTable.Size = new System.Drawing.Size(176, 192);
			this.textBoxTable.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(198, 137);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(79, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Lexem classes:";
			// 
			// textBoxSyntaxicalAnalizerOutput
			// 
			this.textBoxSyntaxicalAnalizerOutput.Location = new System.Drawing.Point(390, 42);
			this.textBoxSyntaxicalAnalizerOutput.Multiline = true;
			this.textBoxSyntaxicalAnalizerOutput.Name = "textBoxSyntaxicalAnalizerOutput";
			this.textBoxSyntaxicalAnalizerOutput.ReadOnly = true;
			this.textBoxSyntaxicalAnalizerOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBoxSyntaxicalAnalizerOutput.Size = new System.Drawing.Size(606, 304);
			this.textBoxSyntaxicalAnalizerOutput.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(387, 15);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(94, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Syntactic analysis:";
			// 
			// buttonSyntaxicAnalize
			// 
			this.buttonSyntaxicAnalize.Location = new System.Drawing.Point(921, 10);
			this.buttonSyntaxicAnalize.Name = "buttonSyntaxicAnalize";
			this.buttonSyntaxicAnalize.Size = new System.Drawing.Size(75, 23);
			this.buttonSyntaxicAnalize.TabIndex = 9;
			this.buttonSyntaxicAnalize.Text = "Analize";
			this.buttonSyntaxicAnalize.UseVisualStyleBackColor = true;
			this.buttonSyntaxicAnalize.Click += new System.EventHandler(this.buttonSyntaxicAnalize_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1008, 367);
			this.Controls.Add(this.buttonSyntaxicAnalize);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textBoxSyntaxicalAnalizerOutput);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBoxTable);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBoxOutput);
			this.Controls.Add(this.buttonParse);
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.textBoxInput);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBoxInput;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button buttonParse;
		private System.Windows.Forms.TextBox textBoxOutput;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxTable;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxSyntaxicalAnalizerOutput;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonSyntaxicAnalize;
	}
}

