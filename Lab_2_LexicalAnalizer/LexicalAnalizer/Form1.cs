﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LexicalAnalizerModels.BusinessLogic;
using SintaxicalAnalizerModels.BusinessLogic;

namespace LexicalAnalizer
{
	public partial class Form1 : Form
	{
		private LexicalAnalizerService LexicalAnalizer;
		private SyntaxicalAnalizerService SyntaxicalAnalizer;

		private List<Lexem> outputLexems;

		public Form1()
		{
			InitializeComponent();
			LexicalAnalizer = new LexicalAnalizerService();
			SyntaxicalAnalizer = new SyntaxicalAnalizerService();
		}
		private void buttonClear_Click(object sender, EventArgs e)
		{
			textBoxInput.Text = "";
			textBoxOutput.Text = "";
			outputLexems = null;
			textBoxSyntaxicalAnalizerOutput.Text = "";
		}

		private void buttonParse_Click(object sender, EventArgs e)
		{
			try
			{
				outputLexems = LexicalAnalizer.Analize(textBoxInput.Text);
				textBoxTable.Text = LexicalAnalizer.PrintTables();
				textBoxOutput.Text = LexicalAnalizer.PrintOutputLexems(outputLexems);
			}
			catch(ArgumentException ex)
			{
				textBoxOutput.Text = ex.Message;
			}
		}

		private void buttonSyntaxicAnalize_Click(object sender, EventArgs e)
		{
			outputLexems = LexicalAnalizer.Analize(textBoxInput.Text);
			textBoxSyntaxicalAnalizerOutput.Text = SyntaxicalAnalizer.Analize(outputLexems);
		}
	}
}
