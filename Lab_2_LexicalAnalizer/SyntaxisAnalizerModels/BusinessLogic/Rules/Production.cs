﻿using System.Collections.Generic;
using SintaxicalAnalizerModels.BusinessLogic.Symbols;
using System.Text;

namespace SintaxicalAnalizerModels.BusinessLogic.Rules
{
	class Production
	{
		public List<Symbol> Symbols { get; set; }

		public override string ToString()
		{
			StringBuilder res = new StringBuilder();
			foreach(var symbol in Symbols)
			{
				res.Append(symbol.Value)
					.Append(" ");
			}
			return res.ToString();
		}
	}
}
