﻿using LexicalAnalizerModels.BusinessLogic;
using SintaxicalAnalizerModels.BusinessLogic.Rules;
using SintaxicalAnalizerModels.BusinessLogic.Symbols;
using System.Collections.Generic;

namespace SintaxicalAnalizerModels.BusinessLogic
{
	class ControlTable
	{
		private const int RulesCount = 13;
		private const int FirstFollowSymbolsCount = 16;
		public Dictionary<Symbol, Dictionary<string, Production>> Table { get; set; }

		public ControlTable(Dictionary<string, List<Production>> rules)
		{
			Table = new Dictionary<Symbol, Dictionary<string, Production>>
			{
				{new Symbol("S", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{"do", rules["S"][0] },
					{"#", null } }
				},
				{new Symbol("A", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{"(", rules["A"][0] },
					{LexemType.Identifier.ToString(), rules["A"][0] },
					{LexemType.Constant.ToString(), rules["A"][0] },
					{"while", null },
					{")", null },
					{"#", null } }
				},
				{new Symbol("B", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{"+", rules["B"][0] },
					{"-", rules["B"][1] },
					{"while", null },
					{")", null },
					{"#", null }}
				},
				{new Symbol("M", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{"(", rules["M"][0] },
					{LexemType.Identifier.ToString(), rules["M"][0] },
					{LexemType.Constant.ToString(), rules["M"][0] },
					{"+", null },
					{"-", null },
					{"while", null },
					{")", null },
					{"#", null }}
				},
				{new Symbol("C", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{"+", null },
					{"-", null },
					{"*", rules["C"][0] },
					{"/", rules["C"][1] },
					{"while", null },
					{")", null },
					{"#", null }}
				},
				{new Symbol("P", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{"(", rules["P"][0] },
					{LexemType.Identifier.ToString(), rules["P"][1] },
					{LexemType.Constant.ToString(), rules["P"][2] },
					{"+", null },
					{"-", null },
					{"*", null },
					{"/", null },
					{"while", null },
					{")", null },
					{"#", null }}
				},
				{new Symbol("N", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{LexemType.Identifier.ToString(), rules["N"][0] },
					{"=", null },
					{"#", null }}
				},
				{new Symbol("D", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{"[", rules["D"][0] },
					{"=", null },
					{"#", null }}
				},
				{new Symbol("E", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{LexemType.Identifier.ToString(), rules["E"][0] },
					{LexemType.Constant.ToString(), rules["E"][0] },
					{"]", null },
					{"#", null }}
				},
				{new Symbol("Z", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{ ",", rules["Z"][0] },
					{"]", null },
					{"#", null }}
				},
				{new Symbol("F", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{LexemType.Identifier.ToString(), rules["F"][0] },
					{LexemType.Constant.ToString(), rules["F"][1] },
					{",", null },
					{"]", null },
					{"#", null }}
				},
				{new Symbol("X", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{LexemType.Identifier.ToString(), rules["X"][0] },
					{")", null },
					{"#", null }}
				},
				{new Symbol("Y", SymbolType.Nonterminal), new Dictionary<string, Production> {
					{LexemType.Constant.ToString(), null },
					{">", rules["Y"][0] },
					{"<", rules["Y"][1] },
					{"#", null }}
				}
			};
		}
	}
}
