﻿namespace SintaxicalAnalizerModels.BusinessLogic.Symbols
{
	public class Symbol
	{
		public string Value { get; set; }
		public SymbolType Type { get; set; }

		public Symbol(string val, SymbolType type)
		{
			Value = val;
			Type = type;
		}

		public override int GetHashCode()
		{
			int res = 0;
			foreach (char c in Value)
			{
				res += (int)c;
			}
			return res;
		}
		public override bool Equals(object obj)
		{
			return Equals(obj as Symbol);
		}
		public bool Equals(Symbol obj)
		{
			return obj != null && obj.Value == this.Value;
		}
	}
}
