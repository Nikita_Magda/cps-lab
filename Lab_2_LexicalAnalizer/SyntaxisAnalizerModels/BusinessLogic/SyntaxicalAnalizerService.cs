﻿using LexicalAnalizerModels.BusinessLogic;
using SintaxicalAnalizerModels.BusinessLogic.Rules;
using SintaxicalAnalizerModels.BusinessLogic.Symbols;
using System;
using System.Collections.Generic;
using System.Text;

namespace SintaxicalAnalizerModels.BusinessLogic
{
	public class SyntaxicalAnalizerService
	{
		private Dictionary<string, List<Production>> RuleSet { get; set; }

		private ControlTable ControlTable { get; set; }

		private Stack<Symbol> Stack { get; set; }

		public SyntaxicalAnalizerService()
		{
			RuleSet = new Dictionary<string, List<Production>>
			{
				{"S", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("do", SymbolType.Terminal),
								new Symbol("N", SymbolType.Nonterminal),
								new Symbol("=", SymbolType.Terminal),
								new Symbol("A", SymbolType.Nonterminal),
								new Symbol("while", SymbolType.Terminal),
								new Symbol("(", SymbolType.Terminal),
								new Symbol("X", SymbolType.Nonterminal),
								new Symbol(")", SymbolType.Terminal),
								new Symbol("#", SymbolType.Terminal),
							}
						}
					}
				},
				{"A", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("M", SymbolType.Nonterminal),
								new Symbol("B", SymbolType.Nonterminal),
							}
						}
					}
				},
				{"B", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("+", SymbolType.Terminal),
								new Symbol("M", SymbolType.Nonterminal),
								new Symbol("B", SymbolType.Nonterminal),
							}
						},
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("-", SymbolType.Terminal),
								new Symbol("M", SymbolType.Nonterminal),
								new Symbol("B", SymbolType.Nonterminal),
							}
						},
						null
					}
				},
				{"M", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("P", SymbolType.Nonterminal),
								new Symbol("C", SymbolType.Nonterminal),
							}
						}
					}
				},
				{"C", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("*", SymbolType.Terminal),
								new Symbol("P", SymbolType.Nonterminal),
								new Symbol("C", SymbolType.Nonterminal),
							}
						},
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("/", SymbolType.Terminal),
								new Symbol("P", SymbolType.Nonterminal),
								new Symbol("C", SymbolType.Nonterminal),
							}
						},
						null
					}
				},
				{"P", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("(", SymbolType.Terminal),
								new Symbol("A", SymbolType.Nonterminal),
								new Symbol(")", SymbolType.Terminal),
							}
						},
						new Production {
							Symbols = new List<Symbol> {
								new Symbol(LexemType.Identifier.ToString(), SymbolType.Terminal),
							}
						},
						new Production {
							Symbols = new List<Symbol> {
								new Symbol(LexemType.Constant.ToString(), SymbolType.Terminal),
							}
						},
					}
				},
				{"N", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol(LexemType.Identifier.ToString(), SymbolType.Terminal),
								new Symbol("D", SymbolType.Nonterminal),
							}
						},
					}
				},
				{"D", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("[", SymbolType.Terminal),
								new Symbol("E", SymbolType.Nonterminal),
								new Symbol("]", SymbolType.Terminal),
							}
						}
					}
				},
				{"E", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("F", SymbolType.Nonterminal),
								new Symbol("Z", SymbolType.Nonterminal),
							}
						}
					}
				},
				{"Z", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol(",", SymbolType.Terminal),
								new Symbol("F", SymbolType.Nonterminal),
								new Symbol("Z", SymbolType.Nonterminal),
							}
						},
						null
					}
				},
				{"F", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol(LexemType.Identifier.ToString(), SymbolType.Terminal),
							}
						},
						new Production
						{
							Symbols = new List<Symbol> {
								new Symbol(LexemType.Constant.ToString(), SymbolType.Terminal),
							}
						}
					}
				},
				{"X", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol(LexemType.Identifier.ToString(), SymbolType.Terminal),
								new Symbol("Y", SymbolType.Nonterminal),
								new Symbol(LexemType.Constant.ToString(), SymbolType.Terminal),
							}
						}
					}
				},
				{"Y", new List<Production>
					{
						new Production {
							Symbols = new List<Symbol> {
								new Symbol(">", SymbolType.Terminal),
							}
						},
						new Production {
							Symbols = new List<Symbol> {
								new Symbol("<", SymbolType.Terminal),
							}
						}
					}
				},
			};

			ControlTable = new ControlTable(RuleSet);

			Stack = new Stack<Symbol>();
		}

		public string Analize(List<Lexem> lexems)
		{
			Stack.Clear();
			Stack.Push(new Symbol("S", SymbolType.Nonterminal));
			StringBuilder res = new StringBuilder();
			res.Append(PrintAnalizeStep("none", LexicalAnalizerService.LexemTables[lexems[0].Type][lexems[0].Index]));
			foreach (var lexem in lexems)
			{
				Symbol top = Stack.Pop();
				while (top.Type != SymbolType.Terminal)
				{
					Production rule;
					try
					{
						var temp = ControlTable.Table[top];
						if (lexem.Type == LexemType.Constant ||
							lexem.Type == LexemType.Identifier)
						{
							rule = ControlTable.Table[top][lexem.Type.ToString()];
						}
						else rule = ControlTable.Table[top][LexicalAnalizerService.LexemTables[lexem.Type][lexem.Index]];
					}
					catch
					{
						res.Append("Error: incorrect input. There is no such lexem value in the control table.\r\n");
						return res.ToString();
					}
					if(rule != null)
					{
						for (int i = rule.Symbols.Count - 1; i >= 0; i--)
						{
							Stack.Push(rule.Symbols[i]);
						}
					}
					string rightPart = rule == null ? "Null" : rule.ToString();
					res.Append(PrintAnalizeStep($"{top.Value} --> {rightPart}", LexicalAnalizerService.LexemTables[lexem.Type][lexem.Index]));
					top = Stack.Pop();
				}
				if (lexem.Type == LexemType.Constant ||
					lexem.Type == LexemType.Identifier)
				{
					if (top.Value != lexem.Type.ToString())
					{
						res.Append("Error: incorrect input. Wrong rule transformation.\r\n");
						return res.ToString();
					}
				}
				else
				{
					if (top.Value != LexicalAnalizerService.LexemTables[lexem.Type][lexem.Index])
					{ 
						res.Append("Error: incorrect input. Wrong rule transformation.\r\n");
						return res.ToString();
					}
				}
				res.Append(PrintAnalizeStep($"Pop({top.Value})", LexicalAnalizerService.LexemTables[lexem.Type][lexem.Index]));
			}
			res.Append("Success: access allowed.");
			return res.ToString();
		}

		private string PrintAnalizeStep(string action, string current)
		{
			StringBuilder stackState = new StringBuilder();
			foreach (var symbol in Stack)
			{
				stackState.Append(symbol.Value)
						  .Append(", ");
			}
			if(stackState.Length >= 2)
			{
				stackState.Remove(stackState.Length - 2, 2);
			}
			return $"Current input: {current};\t Action: {action};\t Stack: {stackState.ToString()};\t \r\n\r\n";
		}
	}
}
